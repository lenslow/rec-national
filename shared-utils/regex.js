
/* eslint-disable max-len */
// http://stackoverflow.com/a/46181
export const EMAIL_RGX = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-?\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

// Password must be 8+ characters
export const PASSWORD_RGX = /^.{8,}$/;
