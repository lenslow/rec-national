
exports.seed = (knex, Promise) => Promise.all([

    // clear
    knex('artistCategories').del(),

    // insert
    knex('artistCategories').insert({ category: 'Musician' }),
    knex('artistCategories').insert({ category: 'Graphic Designer' }),
    knex('artistCategories').insert({ category: 'Lighting Designer' }),
    knex('artistCategories').insert({ category: 'Sound Engineer' }),
]);
