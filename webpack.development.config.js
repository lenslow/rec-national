var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    entry: [
        'webpack-dev-server/client?http://localhost:8081/',
        'webpack/hot/only-dev-server',
        './client/index',
    ],

    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, '/dist/'),
        publicPath: '/',
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.NoErrorsPlugin(),

        new HtmlWebpackPlugin({
            title: 'RASP',
            filename: 'index.html',
            template: './client/index.html',
        }),
    ],

    resolve: {
        extensions: ['', '.jsx', '.js', '.json'],
        modulesDirectories: ['node_modules'],
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                },
            },

            {
                test: /\.scss$/i,
                loader: 'style!css!sass?' + 'includePaths[]=' + (path.resolve(__dirname, './node_modules')),
            },

            {
                test: /\.(eot|otf|svg|ttf|woff|woff2)$/i,
                loader: 'file-loader?name=fonts/[name].[ext]',
            },

            {
                test: /no-javascript\.css/i,
                loader: 'file-loader?name=[name].[ext]',
            },
        ],
    },

    devtool: 'source-map',
};
