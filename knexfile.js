module.exports = {
    development: {
        client: 'pg',
        connection: {
            database: 'raspy',
        },
        debug: true,
    },
};
