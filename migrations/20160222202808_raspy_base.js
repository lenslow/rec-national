exports.up = (knex, Promise) => Promise.all([
    knex.schema.createTable('users', table => {
        table.increments('id').primary();
        table.timestamp('createdAt').notNullable().defaultTo(knex.raw('now()'));
        table.timestamp('updatedAt');

        table.string('email').unique();
        table.string('password');
        table.string('salt');

        // personal information
        table.string('firstName');
        table.string('lastName');
        table.string('phoneNumber');

        // user flags
        table.boolean('hasLoggedIn').defaultTo(false);
        table.boolean('hasVerifiedEmail').defaultTo(false);

        // user specific location (for defaulting search and other stuff)
        table.integer('locationId').references('id').inTable('locations');

        // artist table will hold user specific, artist related information
        table.integer('artistId').references('id').inTable('artist').unique();

        // service provider table will hold the associated service provider account
        table.integer('serviceProviderId').references('id').inTable('serviceProvider').unique();
    }),

    knex.schema.createTable('artist', table => {
        table.increments('id').primary();
        table.string('name');
        table.string('email').unique();
        table.text('about');

        table.integer('artistCategoryId').references('id').inTable('artistCategories');
        table.integer('artistGenreId').references('id').inTable('artistGenres');
        table.integer('locationId').references('id').inTable('locations'); // artist specific location
    }),

    knex.schema.createTable('artistCategories', table => {
        table.increments('id').primary();
        table.string('category');
    }),

    knex.schema.createTable('artistGenres', table => {
        table.increments('id').primary();
        table.string('genre');
    }),

    knex.schema.createTable('serviceProvider', table => {
        table.increments('id').primary();
        table.string('name');
        table.text('about');

        table.integer('locationId').references('id').inTable('locations'); // service provider specific location
    }),

    knex.schema.createTable('service', table => {
        table.increments('id').primary();
        table.string('name');
        table.string('description');

        table.integer('serviceProviderId').references('id').inTable('service_provider');
        table.integer('locationId').references('id').inTable('locations'); // service specific location
    }),

    knex.schema.createTable('locations', table => {
        table.increments('id').primary();
        table.string('zipcode');
    }),
]);

exports.down = (knex, Promise) => Promise.all([
    knex.schema.dropTable('user'),
    knex.schema.dropTable('artist'),
    knex.schema.dropTable('artistCategories'),
    knex.schema.dropTable('artistGenres'),

    knex.schema.dropTable('serviceProvider'),
    knex.schema.dropTable('service'),

    knex.schema.dropTable('locations'),
]);
