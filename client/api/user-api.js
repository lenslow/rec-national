import $ from 'jquery';
import configuration from '../util/configuration';
const path = `${configuration.api}/user`;

export function updateUser(userId, data) {
    return new Promise((resolve, reject) => {
        $.ajax(`${path}/${userId}`, {
            method: 'PATCH',
            data: JSON.stringify(data),
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(error),
        });
    });
}
