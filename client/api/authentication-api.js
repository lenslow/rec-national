import $ from 'jquery';
import configuration from '../util/configuration';
const path = `${configuration.api}/authentication`;

export function loginUser(email, password) {

    const data = {
        email,
        password,
    };

    return new Promise((resolve, reject) => {
        $.ajax(`${path}/login`, {
            method: 'POST',
            data: JSON.stringify(data),
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(xhr.responseJSON),
        });
    });
}

export function userRequested() {
    return new Promise((resolve, reject) => {
        $.ajax(`${path}/user`, {
            method: 'GET',
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(error),
        });
    });
}

export function registerUser(email, password, confirmPassword) {
    const data = {
        email,
        password,
        confirmPassword,
    };

    return new Promise((resolve, reject) => {
        $.ajax(`${path}/register`, {
            method: 'POST',
            data: JSON.stringify(data),
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(xhr.responseJSON),
        });
    });
}

export function requestPasswordReset(email) {
    const data = {
        email,
    };

    return new Promise((resolve, reject) => {
        $.ajax(`${path}/request-password-reset`, {
            method: 'POST',
            data: JSON.stringify(data),
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(error),
        });
    });
}

export function resetPassword(newPassword, email, resetToken) {
    const data = {
        newPassword,
        email,
        resetToken,
    };

    return new Promise((resolve, reject) => {
        $.ajax(`${path}/password-reset`, {
            method: 'POST',
            data: JSON.stringify(data),
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(error),
        });
    });
}

export function verifyEmail(emailVerificationToken, email) {
    const data = {
        emailVerificationToken,
        email,
    };

    return new Promise((resolve, reject) => {
        $.ajax(`${path}/verify-email`, {
            method: 'POST',
            data: JSON.stringify(data),
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(error),
        });
    });
}
