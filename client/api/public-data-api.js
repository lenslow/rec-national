import $ from 'jquery';
import configuration from '../util/configuration';
const path = `${configuration.api}/public-data`;

export function getProfileUser(userId) {
    return new Promise((resolve, reject) => {
        $.ajax(`${path}/${userId}`, {
            method: 'GET',
            success: (res, status, xhr) => resolve(res),
            error: (xhr, status, error) => reject(error),
        });
    });
}
