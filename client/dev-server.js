// webpack dev server

import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import config from '../webpack.development.config.js';

new WebpackDevServer(webpack(config), {
    hot: true,
    historyApiFallback: true,
}).listen(8081, 'localhost', (err, result) => {

    if (err) {
        console.log(err);
    }

    console.log('Listening at localhost:8081');
});
