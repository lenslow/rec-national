
/**
 * Browser check.
 * Unsupported Browser List:
 * - Internet Explorer Below (and including) 9
 *
 * TODO: Test this
 */
export function isSupportedBrowser() {
    let isSupported = true;

    const isIE = isInternetExplorer();
    if (isIE && isIE < 10) {
        isSupported = false;
    }

    return isSupported;
}

// If IE, returns IE version number
function isInternetExplorer() {
    const userAgent = window.navigator.userAgent.toLowerCase();
    return userAgent.indexOf('msie') > -1 ?
        parseInt(userAgent.split('msie')[1], 10) :
        false;
}
