import $ from 'jquery';
import * as storage from './local-storage';

export default function configureAjax() {

    $.ajaxSetup({
        contentType: 'application/json',
    });

    $(document).ajaxSend((e, xhr) => {
        const token = storage.getToken('authentication');
        if (token) xhr.setRequestHeader('Authorization', token);
    });

    $(document).ajaxError((e, xhr) => {
        const status = xhr.status;
        if (storage.getToken('authentication') && status === 401) {
            storage.clearToken('authentication');
            window.location = '/';
        }
    });
}
