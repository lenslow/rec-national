const storage = window.localStorage;
const authenticationTokenKey = 'rec-token';
const emailVerificationTokenKey = 'rec-evt';

export function setToken(type, token) {
    storage.setItem(getTokenKey(type), token);
}

export function getToken(type) {
    return storage.getItem(getTokenKey(type));
}

export function clearToken(type) {
    storage.removeItem(getTokenKey(type));
}

function getTokenKey(type) {
    switch (type) {
        case 'authentication':
            return authenticationTokenKey;
        case 'emailVerification':
            return emailVerificationTokenKey;
        default:
            return authenticationTokenKey;
    }
}
