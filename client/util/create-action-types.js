
export default function createActionTypes(actions) {
    return actions.reduce((prev, curr, i) => {
        return {
            ...prev,
            [curr]: curr,
        };
    }, {});
}
