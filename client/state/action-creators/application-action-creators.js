import { applicationActionTypes } from '../action-types';
import { authenticationActions } from './';
import * as storage from '../../util/local-storage';
import { routeActions } from 'react-router-redux';
import { isSupportedBrowser } from '../../util/browser-util';

export function applicationLoaded() {
    return dispatch => {
        /**
         * Catch any unsupported browsers and send away to unsupported page.
         * Else, load the application as usual.
         */
        if (!isSupportedBrowser()) {
            dispatch(routeActions.push('/unsupported'));
        } else {
            dispatch({
                type: applicationActionTypes.APPLICATION_LOADED,
            });

            const token = storage.getToken('authentication');
            if (token) {
                dispatch(authenticationActions.userRequested());
            }
        }
    };
}
