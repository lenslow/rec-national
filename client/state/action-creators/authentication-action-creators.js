import { authenticationActionTypes } from '../action-types';
import { authenticationApi } from '../../api';
import * as storage from '../../util/local-storage';

export function userRequested() {
    return dispatch => {
        dispatch({
            type: authenticationActionTypes.USER_REQUESTED,
        });

        authenticationApi.userRequested()
            .then(
                res => dispatch(userRequestSucceeded(res)),
                error => dispatch(userRequestFailed(error)),
            );
    };
}

export function userRequestSucceeded(result) {
    const { token, user } = result;

    return dispatch => {
        dispatch({
            type: authenticationActionTypes.USER_REQUEST_SUCCEEDED,
            data: { token, user },
        });

        storage.setToken('authentication', token);
    };
}

export function userRequestFailed(error) {
    storage.clearToken('authentication');
    return {
        type: authenticationActionTypes.USER_REQUEST_FAILED,
        data: { error },
    };
}
