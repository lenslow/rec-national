import createReducer from '../../util/create-reducer';
import { authenticationActionTypes } from '../action-types';
import loginActionTypes from '../../views/login/state/login-action-types';
import globalHeaderActionsTypes from '../../views/global-header/state/global-header-action-types';

const initialState = {
    email: '',
    password: '',
    token: '',
    isLoggedIn: false,

    // Testing -- must add api check for these
    hasArtist: true,
    hasServiceProvider: true,
    userFetchAttempted: false,
};

export default createReducer(initialState, {

    [loginActionTypes.LOGIN_SUCCEEDED](state, action) {
        const { data: { user } } = action;

        return {
            ...state,
            ...user,
            isLoggedIn: true,
        };
    },

    [authenticationActionTypes.USER_REQUEST_SUCCEEDED](state, action) {
        const { data: { user } } = action;

        return {
            ...state,
            ...user,
            isLoggedIn: true,
            userFetchAttempted: true,
        };
    },

});
