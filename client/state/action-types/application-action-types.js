import createActionTypes from '../../util/create-action-types';

const applicationActionTypes = createActionTypes([
    'APPLICATION_LOADED',
]);

export default applicationActionTypes;
