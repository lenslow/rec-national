import createActionTypes from '../../util/create-action-types';

const authenticationActionTypes = createActionTypes([
    'USER_REQUESTED',
    'USER_REQUEST_SUCCEEDED',
    'USER_REQUEST_FAILED',
]);

export default authenticationActionTypes;
