import React, { Component, PropTypes } from 'react';
import { Router, Route, browserHistory, Redirect, IndexRedirect } from 'react-router';
import { applicationActions } from './state/action-creators';
import {
    App,
    Login,
    PasswordReset,
    Register,

    Home,
    About,
    Support,
    Unsupported,

    SecurityWrapper,
    UserPageWrapper,
    Dashboard,
    Inbox,
    ArtistInfo,
    ServiceProviderInfo,
    BasicProfile,
    Account,
    Services,
    PurchaseHistory,
    EmailVerificationHandler,

    PublicUserProfile,
} from './views';
import * as storage from './util/local-storage';
import { isSupportedBrowser } from './util/browser-util';

export default class AppRouter extends Component {

    static propTypes = {
        store: PropTypes.shape({
            dispatch: PropTypes.func.isRequired,
        }).isRequired,
    };

    componentDidMount() {
        const { store } = this.props;
        const { dispatch } = store;
        dispatch(applicationActions.applicationLoaded());
    }

    // if there is a token on a page that should inaccessible by a registered user, send to a secure page
    onEnterInaccessibleByUser = (nextState, replace) => {
        const token = storage.getToken('authentication');
        if (token) replace('user');
    }

    // send supported browser users away from /unsupported
    onEnterUnsupported = (nextState, replace) => {
        if (isSupportedBrowser()) replace('/');
    }

    // basic token check
    // All secure routes are wrapped in a component that renders nothing until user information is available
    isAuthenticated = (nextState, replace) => {
        const token = storage.getToken('authentication');

        // if no token is found, send to home page
        if (!token) replace('/');
    };

    render() {
        return (
            <Router history={browserHistory}>
                <Route path='/unsupported' component={Unsupported} onEnter={this.onEnterUnsupported} />
                <Route component={App}>

                    {/* Static Pages */}
                    <Route path='/' component={Home} />
                    <Route path='/about' component={About} />
                    <Route path='/support' component={Support} />

                    {/* Dynamic Non-Secure Pages */}
                    <Route onEnter={this.onEnterInaccessibleByUser}>
                        <Route path='login' component={Login} />
                        <Route path='password-reset' component={PasswordReset} />
                        <Route path='register' component={Register} />
                    </Route>

                    {/* Dynamic Secured Pages */}
                    <Route onEnter={this.isAuthenticated} component={SecurityWrapper}>
                        <Route path='user' component={UserPageWrapper}>
                            <IndexRedirect to='dashboard' />
                            <Route path='dashboard' component={Dashboard} />
                            <Route path='inbox' component={Inbox} />
                            <Route path='artist-info' component={ArtistInfo} />
                            <Route path='service-provider-info' component={ServiceProviderInfo} />
                            <Route path='basic-profile' component={BasicProfile} />
                            <Route path='account' component={Account} />
                            <Route path='services' component={Services} />
                            <Route path='purchase-history' component={PurchaseHistory} />
                        </Route>

                        <Route path='profile/user/:userId' component={PublicUserProfile} />
                    </Route>

                    {/* Special Pages */}
                    <Route path='email-verification' component={EmailVerificationHandler} />

                    {/* catch all for unmatched routes */}
                    <Redirect from='*' to='/' />

                </Route>
            </Router>
        );
    }
}
