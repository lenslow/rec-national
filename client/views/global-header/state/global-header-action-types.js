import createActionTypes from '../../../util/create-action-types';

const globalHeaderActionTypes = createActionTypes([
    'LOGOUT_SUBMITTED',
]);

export default globalHeaderActionTypes;
