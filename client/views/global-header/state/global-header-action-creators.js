import globalHeaderActionTypes from './global-header-action-types';
import * as storage from '../../../util/local-storage';

export function logoutSubmitted() {
    return dispatch => {
        dispatch({
            type: globalHeaderActionTypes.LOGOUT_SUBMITTED,
        });

        storage.clearToken('authentication');
        window.location = '/';
    };
}
