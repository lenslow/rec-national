import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GlobalHeader from './components/global-header';
import * as globalHeaderActions from './state/global-header-action-creators';

const select = state => ({
    user: state.user,
});

class GlobalHeaderConnector extends Component {
    render() {
        const { dispatch } = this.props;

        return (
            <GlobalHeader
                {...this.props}
                {...bindActionCreators(globalHeaderActions, dispatch)}
            />
        );
    }
}

export default connect(select)(GlobalHeaderConnector);
