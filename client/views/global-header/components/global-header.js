import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Button } from '../../../components';
import './global-header.scss';

export default class GlobalHeader extends Component {

    static propTypes = {
        user: PropTypes.shape({
            isLoggedIn: PropTypes.bool.isRequired,
        }).isRequired,
        logoutSubmitted: PropTypes.func.isRequired,
    };

    getLinks() {
        const { user: { isLoggedIn } } = this.props;
        return isLoggedIn ?
                this.getLoggedInUserLinks() :
                this.getLoggedOutUserLinks();
    }

    getLoggedInUserLinks() {
        const { logoutSubmitted } = this.props;

        return [
            {
                label: 'Log Out',
                onClick: logoutSubmitted,
            },
        ];
    }

    getLoggedOutUserLinks() {
        return [
            {
                label: 'Log In',
                path: '/login',
            },
            {
                label: 'Sign Up',
                path: '/register',
            },
            {
                label: 'About',
                path: '/about',
            },
            {
                label: 'Support',
                path: '/support',
            },
        ];
    }

    renderNavLinks() {
        return this.getLinks().map((linkData, i) => {
            const { label, path, onClick } = linkData;

            if (path) {
                return <Link key={i} className='global-header__nav-link' to={path}>{label}</Link>;
            } else if (onClick) {
                return <Button className='global-header__nav-link' onClick={onClick}>{label}</Button>;
            }
        });
    }

    render() {
        return (
            <header className='global-header'>
                <div className='global-header__inner'>
                    <Link className='global-header__logo' to='/'>RASPy</Link>
                    <nav className='global-header__nav'>
                        {this.renderNavLinks()}
                    </nav>
                </div>
            </header>
        );
    }
}
