import createActionTypes from '../../../util/create-action-types';

const globalFooterActionTypes = createActionTypes([
    'PLACEHOLDER',
]);

export default globalFooterActionTypes;
