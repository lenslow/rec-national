import React, { Component } from 'react';
import { connect } from 'react-redux';
import GlobalFooter from './components/global-footer';

const select = state => ({
    user: state.user,
});

class GlobalFooterConnector extends Component {
    render() {
        return (
            <GlobalFooter
                {...this.props}
            />
        );
    }
}

export default connect(select)(GlobalFooterConnector);
