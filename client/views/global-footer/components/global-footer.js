import React, { Component } from 'react';
import './global-footer.scss';

export default class GlobalFooter extends Component {
    render() {
        return (
            <footer className='global-footer' />
        );
    }
}
