import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as emailVerificationHandlerActions from './state/email-verification-handler-action-creators';
import { routeActions } from 'react-router-redux';
import * as storage from '../../util/local-storage';

function select(state) {
    return {
        user: state.user,
    };
}

class EmailVerificationHandlerConnector extends Component {

    static propTypes = {
        user: PropTypes.shape({
            userFetchAttempted: PropTypes.bool.isRequired,
            isLoggedIn: PropTypes.bool.isRequired,
            hasVerifiedEmail: PropTypes.bool.isRequired,
            email: PropTypes.string.isRequired,
        }).isRequired,

        location: PropTypes.shape({
            query: PropTypes.shape({
                t: PropTypes.string,
            }),
        }).isRequired,
    };

    componentDidMount() {
        /**
         * If there is no token when the page loads,
         * automatically try to do email verification.
         */
        const authToken = storage.getToken('authentication');
        if (!authToken) this.runChecksAndVerify();
    }

    componentDidUpdate(prevProps) {
        // Wait until user fetch occurs to try email verification.
        const { user: { userFetchAttempted } } = this.props;
        if (userFetchAttempted) this.runChecksAndVerify();
    }

    getEmailVerificationToken() {
        const { location: { query: { t: emailVerificationToken } } } = this.props;
        return emailVerificationToken;
    }

    runChecksAndVerify() {
        const { user: { isLoggedIn, hasVerifiedEmail, email }, dispatch } = this.props;
        const emailVerificationToken = this.getEmailVerificationToken();

        /**
         * User already has a verified email, send away.
         */
        if (hasVerifiedEmail) {
            dispatch(routeActions.push('/user'));

        /**
         * User is not logged in but there is a verification token.
         * Store in local storage and send to login.
         */
        } else if (!isLoggedIn && emailVerificationToken) {
            // save to local storage for use after log in
            storage.setToken('emailVerification', emailVerificationToken);
            dispatch(routeActions.push('/login'));

        /**
         * Email verification for a logged in user with a token and email address.
         */
        } else if (emailVerificationToken && email) {
            dispatch(emailVerificationHandlerActions.emailVerificationSubmitted(emailVerificationToken, email));

        /**
         * Send anyone else away.
         */
        } else {
            dispatch(routeActions.push('/'));
        }
    }

    render() {
        const { dispatch } = this.props;
        return (
            <div className='email-verification-handler'>Verifying Email</div>
        );
    }
}

export default connect(select)(EmailVerificationHandlerConnector);
