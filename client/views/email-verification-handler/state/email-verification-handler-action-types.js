import createActionTypes from '../../../util/create-action-types';

const emailVerificationHandlerActionTypes = createActionTypes([
    'EMAIL_VERIFICATION_SUBMITTED',
    'EMAIL_VERIFICATION_SUCCEEDED',
    'EMAIL_VERIFICATION_FAILED',
]);

export default emailVerificationHandlerActionTypes;
