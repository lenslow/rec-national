import emailVerificationHandlerActionTypes from './email-verification-handler-action-types';
import { verifyEmail } from '../../../api/authentication-api';
import { authenticationActions } from '../../../state/action-creators';
import { routeActions } from 'react-router-redux';
import * as storage from '../../../util/local-storage';

export function emailVerificationSubmitted(emailVerificationToken, email) {
    return dispatch => {
        dispatch({
            type: emailVerificationHandlerActionTypes.EMAIL_VERIFICATION_SUBMITTED,
            data: { emailVerificationToken, email },
        });

        verifyEmail(emailVerificationToken, email)
            .then(
                res => dispatch(emailVerificationSucceeded(res)),
                error => dispatch(emailVerificationFailed(error))
            );
    };
}

export function emailVerificationSucceeded() {
    return dispatch => {
        dispatch({
            type: emailVerificationHandlerActionTypes.EMAIL_VERIFICATION_SUCCEEDED,
        });

        storage.clearToken('emailVerification');
        dispatch(authenticationActions.userRequested());
        dispatch(routeActions.push('/user'));

        // TODO: show 'growl' or someting to say that email has been verified
        alert('Email Verified');
    };
}

// TODO: handle error
export function emailVerificationFailed(error) {
    return dispatch => {
        dispatch({
            type: emailVerificationHandlerActionTypes.EMAIL_VERIFICATION_FAILED,
        });

        storage.clearToken('emailVerification');
    };
}
