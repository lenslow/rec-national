import React, { Component, PropTypes } from 'react';
import { UserNav } from '../';

export default class UserPageWrapper extends Component {

    static propTypes = {
        children: PropTypes.node.isRequired,
    };

    render() {
        const { children } = this.props;

        return (
            <div>
                <UserNav />
                {children}
            </div>
        );
    }
}
