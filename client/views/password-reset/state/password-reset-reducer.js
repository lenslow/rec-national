import createReducer from '../../../util/create-reducer';
import passwordResetActionTypes from './password-reset-action-types';

const initialState = {
    requestEmail: '',
    requestComplete: false,

    newPassword: '',
    newPasswordConfirmation: '',
};

export default createReducer(initialState, {

    [passwordResetActionTypes.REQUEST_EMAIL_CHANGED](state, action) {
        const { data: { value } } = action;

        return {
            ...state,
            requestEmail: value,
        };
    },

    [passwordResetActionTypes.REQUEST_SUCCEEDED](state, action) {
        return {
            requestEmail: '',
            requestComplete: true,
        };
    },

    [passwordResetActionTypes.NEW_PASSWORD_CHANGED](state, action) {
        const { data: { value } } = action;

        return {
            ...state,
            newPassword: value,
        };
    },

    [passwordResetActionTypes.NEW_PASSWORD_CONFIRMATION_CHANGED](state, action) {
        const { data: { value } } = action;

        return {
            ...state,
            newPasswordConfirmation: value,
        };
    },

    [passwordResetActionTypes.PASSWORD_RESET_SUCCEEDED](state, action) {
        return {
            ...initialState,
        };
    },

});
