import passwordResetActionTypes from './password-reset-action-types';
import { authenticationApi } from '../../../api';
import { routeActions } from 'react-router-redux';

export function requestEmailChanged(value) {
    return {
        type: passwordResetActionTypes.REQUEST_EMAIL_CHANGED,
        data: { value },
    };
}

export function requestSubmitted(email) {
    return dispatch => {
        dispatch({
            type: passwordResetActionTypes.REQUEST_SUBMITTED,
            data: { email },
        });

        authenticationApi.requestPasswordReset(email)
            .then(
                res => dispatch(requestSucceeded(res)),
                error => dispatch(requestFailed(error))
            );
    };
}

export function requestSucceeded(res) {
    return {
        type: passwordResetActionTypes.REQUEST_SUCCEEDED,
        data: { res },
    };
}

export function requestFailed(error) {
    return {
        type: passwordResetActionTypes.REQUEST_FAILED,
        data: { error },
    };
}

export function newPasswordChanged(value) {
    return {
        type: passwordResetActionTypes.NEW_PASSWORD_CHANGED,
        data: { value },
    };
}

export function newPasswordConfirmationChanged(value) {
    return {
        type: passwordResetActionTypes.NEW_PASSWORD_CONFIRMATION_CHANGED,
        data: { value },
    };
}

export function passwordResetSubmitted(newPassword, email, resetToken) {
    return dispatch => {
        dispatch({
            type: passwordResetActionTypes.PASSWORD_RESET_SUBMITTED,
            data: { newPassword, email, resetToken },
        });

        authenticationApi.resetPassword(newPassword, email, resetToken)
            .then(
                res => dispatch(passwordResetSucceeded(res)),
                error => dispatch(passwordResetFailed(error))
            );
    };
}

export function passwordResetSucceeded(res) {
    return dispatch => {
        dispatch({
            type: passwordResetActionTypes.PASSWORD_RESET_SUCCEEDED,
            data: { res },
        });

        dispatch(routeActions.push('/login'));
    };
}

export function passwordResetFailed(error) {
    return {
        type: passwordResetActionTypes.PASSWORD_RESET_FAILED,
        data: { error },
    };
}
