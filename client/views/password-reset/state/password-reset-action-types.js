import createActionTypes from '../../../util/create-action-types';

const passwordResetActionTypes = createActionTypes([
    'REQUEST_EMAIL_CHANGED',
    'REQUEST_SUBMITTED',
    'REQUEST_SUCCEEDED',
    'REQUEST_FAILED',

    'NEW_PASSWORD_CHANGED',
    'NEW_PASSWORD_CONFIRMATION_CHANGED',
    'PASSWORD_RESET_SUBMITTED',
    'PASSWORD_RESET_SUCCEEDED',
    'PASSWORD_RESET_FAILED',
]);

export default passwordResetActionTypes;
