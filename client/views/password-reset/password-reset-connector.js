import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PasswordReset from './components/password-reset';
import * as passwordResetActions from './state/password-reset-action-creators';

function select(state) {
    return {
        passwordReset: state.passwordReset,
        routing: state.routing,
    };
}

class PasswordResetConnector extends Component {
    render() {
        const { dispatch } = this.props;

        return (
            <PasswordReset
                {...this.props}
                {...bindActionCreators(passwordResetActions, dispatch)}
            />
        );
    }
}

export default connect(select)(PasswordResetConnector);
