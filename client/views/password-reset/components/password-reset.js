import React, { Component, PropTypes } from 'react';
import { Form, AdministrationContainer } from '../../../components';

export default class PasswordReset extends Component {

    static propTypes = {
        routing: PropTypes.object.isRequired,

        passwordReset: PropTypes.shape({
            requestEmail: PropTypes.string.isRequired,
            newPassword: PropTypes.string.isRequired,
            newPasswordConfirmation: PropTypes.string.isRequired,
        }).isRequired,

        requestEmailChanged: PropTypes.func.isRequired,
        requestSubmitted: PropTypes.func.isRequired,

        newPasswordChanged: PropTypes.func.isRequired,
        newPasswordConfirmationChanged: PropTypes.func.isRequired,
        passwordResetSubmitted: PropTypes.func.isRequired,
    };

    getQueryParams() {
        const { routing: { location: { query: { email, resetToken } = {} } } } = this.props;

        return {
            email: email && decodeURIComponent(email),
            resetToken: resetToken && decodeURIComponent(resetToken),
        };
    }

    handleRequestEmailChange = (value) => {
        const { requestEmailChanged } = this.props;
        requestEmailChanged(value);
    };

    handlePasswordResetRequest = (e) => {
        e.preventDefault();

        const { requestSubmitted, passwordReset: { requestEmail } } = this.props;
        requestSubmitted(requestEmail);
    };

    handleNewPasswordChange = (value) => {
        const { newPasswordChanged } = this.props;
        newPasswordChanged(value);
    };

    handleNewPasswordConfirmationChange = (value) => {
        const { newPasswordConfirmationChanged } = this.props;
        newPasswordConfirmationChanged(value);
    };

    handlePasswordReset = (e) => {
        e.preventDefault();

        const { email, resetToken } = this.getQueryParams();
        const { passwordReset: { newPassword }, passwordResetSubmitted } = this.props;

        passwordResetSubmitted(newPassword, email, resetToken);
    }

    renderRequestForm() {
        const { passwordReset: { requestEmail, requestComplete } } = this.props;
        const fields = [
            {
                id: 'email',
                label: 'Email Address',
                value: requestEmail,
                onChange: this.handleRequestEmailChange,
            },
        ];

        return (
            <div className='password-reset__request'>
                <p className='password-reset__request__message'>You will receive an email with instructions to reset your password.</p>

                {
                    requestComplete ?
                        <p className='password-reset__request__email-sent'>Check your email.</p> :
                        <Form
                            fields={fields}
                            onSubmit={this.handlePasswordResetRequest}
                            submitLabel='Send Email'
                        />
                }
            </div>
        );
    }

    renderResetPasswordForm() {
        const { passwordReset: { newPassword, newPasswordConfirmation } } = this.props;

        const fields = [
            {
                id: 'newPassword',
                label: 'New Password',
                value: newPassword,
                onChange: this.handleNewPasswordChange,
            },
            {
                id: 'newPasswordConfirmation',
                label: 'New Password Confirmation',
                value: newPasswordConfirmation,
                onChange: this.handleNewPasswordConfirmationChange,
            },
        ];

        return (
            <div className='password-reset__reset'>
                <Form
                    fields={fields}
                    onSubmit={this.handlePasswordReset}
                    submitLabel='Reset Password'
                />
            </div>
        );
    }

    render() {
        const { email, resetToken } = this.getQueryParams();
        const isReset = email && resetToken;

        return (
            <AdministrationContainer title={isReset ? 'Reset Your Password' : 'Password Reset Request'}>
                <div className='password-reset'>
                    {
                        (isReset) ?
                            this.renderResetPasswordForm() :
                            this.renderRequestForm()
                    }
                </div>
            </AdministrationContainer>
        );
    }
}
