import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PublicUserProfile from './components/public-user-profile';
import * as publicUserProfileActions from './state/public-user-profile-action-creators';

function select(state) {
    return {
        publicUserProfile: state.publicUserProfile,
    };
}

class PublicUserProfileConnector extends Component {

    static propTypes = {
        params: PropTypes.shape({
            userId: PropTypes.string.isRequired,
        }).isRequired,
    }

    componentDidMount() {
        const { dispatch, params: { userId } } = this.props;

        if (!userId) {
            // TODO: do something
        }

        dispatch(publicUserProfileActions.profileUserRequested(userId));
    }

    render() {
        const { dispatch } = this.props;
        return (
            <PublicUserProfile
                {...this.props}
                {...bindActionCreators(publicUserProfileActions, dispatch)}
            />
        );
    }
}

export default connect(select)(PublicUserProfileConnector);
