import publicUserProfileActionTypes from './public-user-profile-action-types';
import { publicDataApi } from '../../../api';

export function profileUserRequested(userId) {
    return dispatch => {
        dispatch({
            type: publicUserProfileActionTypes.PROFILE_USER_REQUESTED,
            data: { userId },
        });

        publicDataApi.getProfileUser(userId)
            .then(
                publicFacingUser => dispatch(profileUserRequestSucceeded(publicFacingUser)),
                error => dispatch(profileUserRequestFailed(error)),
            );
    };
}

export function profileUserRequestSucceeded(publicFacingUser) {
    return {
        type: publicUserProfileActionTypes.PROFILE_USER_REQUEST_SUCCEEDED,
        data: { publicFacingUser },
    };
}

export function profileUserRequestFailed(error) {
    return {
        type: publicUserProfileActionTypes.PROFILE_USER_REQUEST_FAILED,
        data: { error },
    };
}
