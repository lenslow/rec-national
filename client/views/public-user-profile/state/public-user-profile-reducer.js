import createReducer from '../../../util/create-reducer';
import publicUserProfileActionTypes from './public-user-profile-action-types';

const initialState = {
    publicFacingUser: {},
};

export default createReducer(initialState, {
    [publicUserProfileActionTypes.PROFILE_USER_REQUEST_SUCCEEDED](state, action) {
        const { data: { publicFacingUser } } = action;
        return {
            ...state,
            publicFacingUser: { ...publicFacingUser },
        };
    },
});
