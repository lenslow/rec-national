import createActionTypes from '../../../util/create-action-types';

const publicUserProfileActionTypes = createActionTypes([
    'PROFILE_USER_REQUESTED',
    'PROFILE_USER_REQUEST_SUCCEEDED',
    'PROFILE_USER_REQUEST_FAILED',
]);

export default publicUserProfileActionTypes;
