import React, { Component, PropTypes } from 'react';
import './public-user-profile.scss';

export default class PublicUserProfile extends Component {
    render() {
        const { publicUserProfile: { publicFacingUser: { firstName } } } = this.props;

        return (
            <div className='public-user-profile'>
                User's First Name: {firstName}
            </div>
        );
    }
}
