import createReducer from '../../../util/create-reducer';
import userNavActionTypes from './user-nav-action-types';

const initialState = {
    activeTab: 'Dashboard',
};

export default createReducer(initialState, {
    [userNavActionTypes.TAB_CHANGED]: (state, action) => {
        const { tab } = action.data;
        return {
            ...state,
            activeTab: tab,
        };
    },
});
