import createActionTypes from '../../../util/create-action-types';

const userNavActionTypes = createActionTypes([
    'TAB_CHANGED',
]);

export default userNavActionTypes;
