import userNavActionTypes from './user-nav-action-types';

export function tabChanged(tab) {
    return {
        type: userNavActionTypes.TAB_CHANGED,
        data: { tab },
    };
}
