export const navLinkList = [
    {
        label: 'Dashboard',
        location: 'dashboard',
    },
    {
        label: 'Inbox',
        location: 'inbox',
    },
    {
        label: 'Artist Info',
        location: 'artist-info',
    },
    {
        label: 'Service Provider Info',
        location: 'service-provider-info',
    },
    {
        label: 'Basic Profile',
        location: 'basic-profile',
    },
    {
        label: 'Account',
        location: 'account',
    },
    {
        label: 'Services',
        location: 'services',
    },
    {
        label: 'Purchase History',
        location: 'purchase-history',
    },
];
