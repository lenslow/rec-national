import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { navLinkList } from './constants';
import './user-nav.scss';

export default class UserNav extends Component {

    static propTypes = {
        hasArtist: PropTypes.bool.isRequired,
        hasServiceProvider: PropTypes.bool.isRequired,
        activeTab: PropTypes.string.isRequired,
        tabChanged: PropTypes.func.isRequired,
    };

    handleTabClicked = (e) => {
        const { tabChanged } = this.props;
        const tab = e.target.id;

        tabChanged(tab);
    };

    renderNavLinks(links) {
        const { hasServiceProvider, hasArtist } = this.props;

        return links.map((link, i) => {
            if ((link.label === 'Artist Info' && !hasArtist) ||
                ((link.label === 'Service Provider Info' || link.label === 'Services') && !hasServiceProvider)) {
                return false;
            }

            return (
                <Link
                    key={i}
                    className={'user-nav__nav-link'}
                    to={`/user/${link.location}`}
                    onClick={this.handleTabClicked}
                    activeClassName='user-nav__nav-link--active'
                    id={link.label}
                >
                    {link.label}
                </Link>
            );
        });
    }

    render() {
        return (
            <div className='user-nav'>
                <nav className='user-nav__nav'>
                    {this.renderNavLinks(navLinkList)}
                </nav>
            </div>
        );
    }
}
