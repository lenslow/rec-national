import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserNav from './components/user-nav';
import * as userNavActions from './state/user-nav-action-creators';

const select = state => ({
    ...state.userNav,
    hasArtist: state.user.hasArtist,
    hasServiceProvider: state.user.hasServiceProvider,
});

class UserNavConnector extends Component {

    render() {
        const { dispatch } = this.props;

        return (
            <UserNav
                {...this.props}
                {...bindActionCreators(userNavActions, dispatch)}
            />
        );
    }
}

export default connect(select)(UserNavConnector);
