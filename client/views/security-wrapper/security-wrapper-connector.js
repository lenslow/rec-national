import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

function select(state) {
    return {
        user: state.user,
    };
}

class SecurityWrapperConnector extends Component {

    static propTypes = {
        user: PropTypes.object.isRequired,
        children: PropTypes.node.isRequired,
    };

    render() {
        // if there is no user in state, do not render anything
        const { user: { email, token } } = this.props;
        if (!email && !token) return null; // potentially show loader while user request is happening on refresh

        const { children } = this.props;
        return (
            <div>
                {children}
            </div>
        );
    }
}

export default connect(select)(SecurityWrapperConnector);
