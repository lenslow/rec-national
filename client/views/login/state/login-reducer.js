import createReducer from '../../../util/create-reducer';
import loginActionTypes from './login-action-types';

const initialState = {
    email: '',
    password: '',
    error: null,
};

export default createReducer(initialState, {

    [loginActionTypes.LOGIN_EMAIL_CHANGED](state, action) {
        const { data: { email } } = action;
        const { error } = initialState;

        return {
            ...state,
            email,
            error,
        };
    },

    [loginActionTypes.LOGIN_PASSWORD_CHANGED](state, action) {
        const { data: { password } } = action;
        const { error } = initialState;

        return {
            ...state,
            password,
            error,
        };
    },

    [loginActionTypes.LOGIN_SUCCEEDED](state, action) {
        return {
            ...initialState,
        };
    },

    [loginActionTypes.LOGIN_FAILED](state, action) {
        const { data: { error } } = action;
        return {
            ...state,
            error,
        };
    },
});
