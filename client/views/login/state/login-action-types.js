import createActionTypes from '../../../util/create-action-types';

const loginActionTypes = createActionTypes([
    'LOGIN_EMAIL_CHANGED',
    'LOGIN_PASSWORD_CHANGED',

    'LOGIN_SUBMITTED',
    'LOGIN_SUCCEEDED',
    'LOGIN_FAILED',
]);

export default loginActionTypes;
