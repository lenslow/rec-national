import loginActionTypes from './login-action-types';
import { authenticationApi } from '../../../api';
import { routeActions } from 'react-router-redux';
import * as storage from '../../../util/local-storage';
import * as modalActions from '../../../views/modal/state/modal-action-creators';
import { FirstTimeInfo } from '../../../views';
import { authenticationActions } from '../../../state/action-creators';
import { emailVerificationSubmitted } from '../../../views/email-verification-handler/state/email-verification-handler-action-creators';

export function loginEmailChanged(email) {
    return {
        type: loginActionTypes.LOGIN_EMAIL_CHANGED,
        data: { email },
    };
}

export function loginPasswordChanged(password) {
    return {
        type: loginActionTypes.LOGIN_PASSWORD_CHANGED,
        data: { password },
    };
}

export function loginSubmitted(email, password) {
    return dispatch => {
        dispatch({
            type: loginActionTypes.LOGIN_SUBMITTED,
            data: { email, password },
        });

        authenticationApi.loginUser(email, password)
            .then(
                res => dispatch(loginSucceeded(res)),
                error => dispatch(loginFailed(error)),
            );
    };
}

export function loginSucceeded(result) {
    const { token, user } = result;

    return dispatch => {
        dispatch({
            type: loginActionTypes.LOGIN_SUCCEEDED,
            data: { token, user },
        });

        storage.setToken('authentication', token);

        // fetch proper user information
        dispatch(authenticationActions.userRequested());

        // email verification if necessary
        const emailVerificationToken = storage.getToken('emailVerification');
        if (emailVerificationToken) {
            dispatch(emailVerificationSubmitted(emailVerificationToken, user.email));
        }

        // If it is the users first time send to home page and open inital modal.
        const { hasLoggedIn } = user;
        if (hasLoggedIn) {
            dispatch(routeActions.push('/user'));
        } else {
            dispatch(routeActions.push('/home'));

            const modalData = {
                content: FirstTimeInfo,
                className: 'first-time',
                locked: true,
            };
            dispatch(modalActions.modalOpened(modalData));
        }
    };
}

export function loginFailed(error) {
    return dispatch => {
        dispatch({
            type: loginActionTypes.LOGIN_FAILED,
            data: { error },
        });

        // for if registration auto login fails
        dispatch(routeActions.push('/login'));
    };
}
