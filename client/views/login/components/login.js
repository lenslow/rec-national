import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { AdministrationContainer, Input, Button } from '../../../components';
import './login.scss';

export default class Login extends Component {

    static propTypes = {
        login: PropTypes.shape({
            email: PropTypes.string.isRequired,
            password: PropTypes.string.isRequired,
            error: PropTypes.object,
        }).isRequired,
        loginEmailChanged: PropTypes.func.isRequired,
        loginPasswordChanged: PropTypes.func.isRequired,
        loginSubmitted: PropTypes.func.isRequired,
    };

    handleEmailChange = (value) => {
        const { loginEmailChanged } = this.props;
        loginEmailChanged(value);
    };

    handlePasswordChange = (value) => {
        const { loginPasswordChanged } = this.props;
        loginPasswordChanged(value);
    };

    handleSubmit = (e) => {
        e.preventDefault();

        const { loginSubmitted, login: { email, password } } = this.props;
        loginSubmitted(email, password);
    };

    renderActions() {
        return (
            <div className='login__actions'>
                <p className='login__remember'>Remember Checkbox</p>
                <Link to='/password-reset' className='login__forgot'>Forgot Password?</Link>
            </div>
        );
    }

    renderNoAccount() {
        return (
            <p className='login__no-account'>Don't have an account?
                <Link to='/register' className='login__signup'>Sign Up</Link>
            </p>
        );
    }

    renderLoginError(err) {
        const { message } = err;
        let errorText = '';

        if (message) {
            errorText = message;
        } else {
            errorText = 'An unknown error occurred. Please try again.';
        }

        return (
            <div className='login__error'>{errorText}</div>
        );
    }

    render() {
        const { login: { email, password, error } } = this.props;

        return (
            <AdministrationContainer title='Log In'>
                <div className='login'>
                    <form onSubmit={this.handleSubmit} className='login__form'>
                        <Input
                            id='email'
                            className='login__email'
                            value={email}
                            onChange={this.handleEmailChange}
                            label='Email Address'
                            hasLabel={false}
                            placeholder='Email Address'
                        />

                        <Input
                            id='password'
                            className='login__password'
                            value={password}
                            onChange={this.handlePasswordChange}
                            label='Password'
                            hasLabel={false}
                            placeholder='Password'
                            type='password'
                        />

                        {this.renderActions()}

                        <Button
                            className='login__submit'
                            text='Log In'
                            onClick={this.handleSubmit}
                            disabled={!!error}
                        />
                        {error ? this.renderLoginError(error) : null}
                    </form>

                    {this.renderNoAccount()}
                </div>
            </AdministrationContainer>
        );
    }
}
