import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Login from './components/login';
import * as loginActions from './state/login-action-creators';
import * as modalActions from '../modal/state/modal-action-creators';

function select(state) {
    return {
        login: state.login,
    };
}

class LoginConnector extends Component {
    render() {
        const { dispatch } = this.props;

        return (
            <Login
                {...this.props}
                {...bindActionCreators(loginActions, dispatch)}
                {...bindActionCreators(modalActions, dispatch)}
            />
        );
    }
}

export default connect(select)(LoginConnector);
