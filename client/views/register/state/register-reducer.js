import createReducer from '../../../util/create-reducer';
import registerActionTypes from './register-action-types';

const initialState = {
    email: '',
    password: '',
    confirmPassword: '',
    error: null,
};

export default createReducer(initialState, {

    [registerActionTypes.REGISTER_EMAIL_CHANGED](state, action) {
        const { data: { email } } = action;
        const { error } = initialState;

        return {
            ...state,
            email,
            error,
        };
    },

    [registerActionTypes.REGISTER_PASSWORD_CHANGED](state, action) {
        const { data: { password } } = action;
        const { error } = initialState;

        return {
            ...state,
            password,
            error,
        };
    },

    [registerActionTypes.REGISTER_CONFIRM_PASSWORD_CHANGED](state, action) {
        const { data: { confirmPassword } } = action;
        const { error } = initialState;

        return {
            ...state,
            confirmPassword,
            error,
        };
    },

    [registerActionTypes.REGISTER_FAILED](state, action) {
        const { data: { error } } = action;
        return {
            ...state,
            error,
        };
    },
});
