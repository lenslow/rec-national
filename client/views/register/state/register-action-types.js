import createActionTypes from '../../../util/create-action-types';

const registerActionTypes = createActionTypes([
    'REGISTER_EMAIL_CHANGED',
    'REGISTER_PASSWORD_CHANGED',
    'REGISTER_CONFIRM_PASSWORD_CHANGED',

    'REGISTER_SUBMITTED',
    'REGISTER_SUCCEEDED',
    'REGISTER_FAILED',
]);

export default registerActionTypes;
