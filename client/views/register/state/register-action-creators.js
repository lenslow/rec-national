import registerActionTypes from './register-action-types';
import * as loginActions from '../../login/state/login-action-creators';
import { authenticationApi } from '../../../api';

export function emailChanged(email) {
    return {
        type: registerActionTypes.REGISTER_EMAIL_CHANGED,
        data: { email },
    };
}

export function passwordChanged(password) {
    return {
        type: registerActionTypes.REGISTER_PASSWORD_CHANGED,
        data: { password },
    };
}

export function confirmPasswordChanged(confirmPassword) {
    return {
        type: registerActionTypes.REGISTER_CONFIRM_PASSWORD_CHANGED,
        data: { confirmPassword },
    };
}

export function registerSubmitted(email, password, confirmPassword) {
    return dispatch => {
        dispatch({
            type: registerActionTypes.REGISTER_SUBMITTED,
            data: { email, password, confirmPassword },
        });

        authenticationApi.registerUser(email, password, confirmPassword)
            .then(
                res => dispatch(registerSucceeded(res, email, password)),
                error => dispatch(registerFailed(error)),
            );
    };
}

export function registerSucceeded(result, email, password) {
    return dispatch => {
        dispatch({
            type: registerActionTypes.REGISTER_SUCCEEDED,
            data: { result },
        });

        // auto log in the new user
        dispatch(loginActions.loginSubmitted(email, password));
    };
}

export function registerFailed(error) {
    return {
        type: registerActionTypes.REGISTER_FAILED,
        data: { error },
    };
}
