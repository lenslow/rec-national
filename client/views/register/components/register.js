import React, { Component, PropTypes } from 'react';
import { Input, Button, AdministrationContainer } from '../../../components';

import './register.scss';

export default class Register extends Component {

    static propTypes = {
        email: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired,
        confirmPassword: PropTypes.string.isRequired,
        emailChanged: PropTypes.func.isRequired,
        passwordChanged: PropTypes.func.isRequired,
        confirmPasswordChanged: PropTypes.func.isRequired,
        registerSubmitted: PropTypes.func.isRequired,
        error: PropTypes.object,
    };

    handleEmailChange = (value) => {
        const { emailChanged } = this.props;
        emailChanged(value);
    };

    handlePasswordChange = (value) => {
        const { passwordChanged } = this.props;
        passwordChanged(value);
    };

    handleConfirmPasswordChange = (value) => {
        const { confirmPasswordChanged } = this.props;
        confirmPasswordChanged(value);
    }

    handleSubmit = (e) => {
        e.preventDefault();

        const { registerSubmitted, email, password, confirmPassword } = this.props;
        registerSubmitted(email, password, confirmPassword);
    };

    renderRegistrationError() {
        const { error: { error, message } } = this.props;
        let errorText = '';

        if (error && error.constraint === 'users_email_unique') {
            errorText = 'An account with that email address already exists.';
        } else if (message) {
            errorText = message;
        } else {
            errorText = 'An unknown error occurred. Please try again.';
        }

        return (
            <div className='register__error'>{errorText}</div>
        );
    }

    render() {
        const { email, password, confirmPassword, error } = this.props;

        return (
            <AdministrationContainer title='Register For RASPY'>
                <div className='register'>
                    <form className='register__form' onSubmit={this.handleSubmit}>
                        <Input
                            id='email'
                            className='register__email'
                            value={email}
                            onChange={this.handleEmailChange}
                            label='Email Address'
                            hasLabel={false}
                            placeholder='Email Address'
                        />
                        <Input
                            id='password'
                            className='register__password'
                            value={password}
                            onChange={this.handlePasswordChange}
                            label='Password'
                            hasLabel={false}
                            placeholder='Password'
                            type='password'
                        />
                        <Input
                            id='confirm-password'
                            className='register__confirm-password'
                            value={confirmPassword}
                            onChange={this.handleConfirmPasswordChange}
                            label='Confirm Password'
                            hasLabel={false}
                            placeholder='Confirm Password'
                            type='password'
                        />
                        <Button
                            className='register__submit'
                            text='Register'
                            onClick={this.handleSubmit}
                            disabled={!!error}
                        />
                        {error ? this.renderRegistrationError() : null}
                    </form>
                </div>
            </AdministrationContainer>
        );
    }
}
