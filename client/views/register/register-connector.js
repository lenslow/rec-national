import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Register from './components/register';
import * as registerActions from './state/register-action-creators';

const select = state => ({
    ...state.register,
});

class RegisterConnector extends Component {
    render() {
        const { dispatch } = this.props;

        return (
            <Register
                {...this.props}
                {...bindActionCreators(registerActions, dispatch)}
            />
        );
    }
}

export default connect(select)(RegisterConnector);
