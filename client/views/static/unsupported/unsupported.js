import React, { Component } from 'react';

// TODO: Create unsupported page

export default class Unsupported extends Component {
    render() {
        return (
            <div className='unsupported'>
                You have an unsupported browser.
            </div>
        );
    }
}
