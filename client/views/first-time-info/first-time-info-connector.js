import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FirstTimeInfo from './components/first-time-info';
import * as firstTimeInfoActions from './state/first-time-info-action-creators';
import * as modalActions from '../modal/state/modal-action-creators';

function select(state) {
    return {
        firstTimeInfo: state.firstTimeInfo,
    };
}

function FirstTimeInfoConnector(props) {
    const { dispatch } = props;
    return (
        <FirstTimeInfo
            {...props}
            {...bindActionCreators(firstTimeInfoActions, dispatch)}
            {...bindActionCreators(modalActions, dispatch)}
        />
    );
}

export default connect(select)(FirstTimeInfoConnector);
