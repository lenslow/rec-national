import createReducer from '../../../util/create-reducer';
import firstTimeInfoActionTypes from './first-time-info-action-types';

const initialState = {
    step: 0,

    firstName: '',
    lastName: '',
    phoneNumber: '',
    zipCode: '', // TODO: handle locations
};

export default createReducer(initialState, {
    [firstTimeInfoActionTypes.INPUT_CHANGED](state, action) {
        const { data: { key, value } } = action;

        return {
            ...state,
            [key]: value,
        };
    },

    [firstTimeInfoActionTypes.STEP_CHANGED](state) {
        return {
            ...state,
            step: state.step + 1,
        };
    },

    [firstTimeInfoActionTypes.UPDATE_PERSONAL_INFO_SUCCEEDED](state) {
        return {
            ...state,
            step: 1,
        };
    },
});
