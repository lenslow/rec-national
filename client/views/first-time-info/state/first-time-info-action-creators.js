import firstTimeInfoActionTypes from './first-time-info-action-types';
import { updateUser } from '../../../api/user-api';
import { authenticationActions } from '../../../state/action-creators';
import * as modalActions from '../../../views/modal/state/modal-action-creators';

export function stepChanged() {
    return dispatch => {
        dispatch({
            type: firstTimeInfoActionTypes.STEP_CHANGED,
        });
        dispatch(modalActions.modalUnlocked());
    };
}

export function inputChanged(key, value) {
    return {
        type: firstTimeInfoActionTypes.INPUT_CHANGED,
        data: { key, value },
    };
}

export function personalInfoSubmitted(firstName, lastName, phoneNumber) {
    return (dispatch, getState) => {
        dispatch({
            type: firstTimeInfoActionTypes.PERSONAL_INFO_SUBMITTED,
        });

        const { user: { userId } } = getState();
        const data = {
            firstName,
            lastName,
            phoneNumber,
        };

        updateUser(userId, data)
            .then(
                res => dispatch(updatePersonalInfoSucceeded(res)),
                error => dispatch(updatePersonalInfoFailed(error))
            );
    };
}

export function updatePersonalInfoSucceeded() {
    return dispatch => {
        dispatch({
            type: firstTimeInfoActionTypes.UPDATE_PERSONAL_INFO_SUCCEEDED,
        });

        dispatch(authenticationActions.userRequested());
        dispatch(modalActions.modalUnlocked());
    };
}

export function updatePersonalInfoFailed(error) {
    return {
        type: firstTimeInfoActionTypes.UPDATE_PERSONAL_INFO_FAILED,
        data: { error },
    };
}
