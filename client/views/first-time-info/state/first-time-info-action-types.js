import createActionTypes from '../../../util/create-action-types';

const firstTimeInfoActionTypes = createActionTypes([
	'STEP_CHANGED',
    'INPUT_CHANGED',

    'PERSONAL_INFO_SUBMITTED',
    'UPDATE_PERSONAL_INFO_SUCCEEDED',
    'UPDATE_PERSONAL_INFO_FAILED',
]);

export default firstTimeInfoActionTypes;
