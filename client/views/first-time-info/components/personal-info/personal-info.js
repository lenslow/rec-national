import React, { Component, PropTypes } from 'react';
import { Input, Button } from '../../../../components';

export default class PersonalInfo extends Component {

    handleSubmit = e => {
        e.preventDefault();

        const {
            firstTimeInfo: { firstName, lastName, phoneNumber },
            personalInfoSubmitted,
        } = this.props;

        personalInfoSubmitted(firstName, lastName, phoneNumber);
    };

    renderTitles() {
        return (
            <div className='personal-info__titles'>
                <h2>Thank you for registering!</h2>
                <p>Get started by providing some information about yourself</p>
            </div>
        );
    }

    renderForm() {
        const {
            firstTimeInfo: { firstName, lastName, phoneNumber },
            inputChanged,
        } = this.props;

        return (
            <form onSubmit={this.handleSubmit}>
                <Input
                    id='firstName'
                    className='personal-info__first-name'
                    value={firstName}
                    onChange={(value) => inputChanged('firstName', value)}
                    label='First Name'
                    hasLabel={false}
                    placeholder='First Name'
                />

                <Input
                    id='lastName'
                    className='personal-info__last-name'
                    value={lastName}
                    onChange={(value) => inputChanged('lastName', value)}
                    label='Last Name'
                    hasLabel={false}
                    placeholder='Last Name'
                />

                {/* TODO: validate phone numbers */}
                <Input
                    id='phoneNumber'
                    className='personal-info__phone-number'
                    value={phoneNumber}
                    onChange={(value) => inputChanged('phoneNumber', value)}
                    label='Phone Number'
                    hasLabel={false}
                    placeholder='Phone Number'
                />

                <Button
                    onClick={this.handleSubmit}
                    text='Submit Information'
                />
            </form>
        );
    }

    render() {
        return (
            <div className='personal-info'>
                {this.renderTitles()}
                {this.renderForm()}
            </div>
        );
    }
}
