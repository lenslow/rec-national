import React, { Component, PropTypes } from 'react';
import './first-time-info.scss';
import { Button } from '../../../components';
import PersonalInfo from './personal-info/personal-info';
import ExtendedProfileSelection from './extended-profile-selection/extended-profile-selection';

export default class FirstTime extends Component {

    renderStep() {
        const { firstTimeInfo: { step } } = this.props;
        switch (step) {
            case 0:
            default:
                return <PersonalInfo {...this.props} />;
            case 1:
                return <ExtendedProfileSelection {...this.props} />;

        }
    }

    renderActions() {
        const { firstTimeInfo: { step }, modalClosed, stepChanged } = this.props;

        return (
            <div className='first-time-info__actions'>
                <Button
                    className='first-time-info__start'
                    onClick={modalClosed}
                    type='text'
                    text='Start Exploring'
                />

                <Button
                    className='first-time-info__skip'
                    onClick={stepChanged}
                    type='text'
                    text='Skip this step'
                    hidden={step > 0}
                />
            </div>
        );
    }

    render() {
        return (
            <div className='first-time-info'>
                {this.renderStep()}
                {this.renderActions()}
            </div>
        );
    }
}
