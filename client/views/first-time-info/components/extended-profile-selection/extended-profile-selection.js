import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

export default class ExtendedProfileSelection extends Component {
    render() {
        const { modalClosed } = this.props;
        return (
            <div className='extended-profile-selection'>
                <Link to='/user/artist-info' onClick={modalClosed}>Enter Artist Information</Link>

                {/* Temporary... not sure where this is going yet */}
                <Link to='/user/service-provider-info' onClick={modalClosed}>Enter Servce Provider Information</Link>
            </div>
        );
    }
}
