import React, { Component, PropTypes } from 'react';
import $ from 'jquery';
import './modal.scss';

export default class Modal extends Component {

    static propTypes = {
        modal: PropTypes.shape({
            isOpen: PropTypes.bool.isRequired,
            content: PropTypes.func,
            className: PropTypes.string,
            locked: PropTypes.bool,
        }),

        modalClosed: PropTypes.func.isRequired,
    };

    componentDidUpdate(prevProps) {
        const { modal: { isOpen: wasOpen } } = prevProps;
        const { modal: { isOpen } } = this.props;

        if (isOpen && isOpen !== wasOpen) {
            document.addEventListener('click', this.handleDocumentClick);
            document.addEventListener('keyup', this.handleDocumentKeyUp);
        } else if (wasOpen && !isOpen) {
            document.removeEventListener('click', this.handleDocumentClick);
            document.removeEventListener('keyup', this.handleDocumentKeyUp);
        }
    }

    handleDocumentClick = (e) => {
        const { modal: { locked } } = this.props;
        if (locked) return;

        const isClickInModal = $.contains(this.refs.box, e.target) || e.target === this.refs.box;
        if (isClickInModal) return;

        this.closeModal();
    };

    handleDocumentKeyUp = (e) => {
        const { modal: { locked } } = this.props;
        const key = e.keyCode || e.which;

        if (key === 27 && !locked) this.closeModal(); // close with escape key
    };

    closeModal() {
        const { modalClosed } = this.props;
        modalClosed();
    }

    renderOverlay() {
        return <div className='modal__overlay' />;
    }

    renderBox() {
        const { modal: { content, className } } = this.props;
        const classes = `modal__box ${className}`;
        const Content = content;

        return (
            <div className={classes} ref='box'>
                <div className='modal__box-inner'>
                    <Content />
                </div>
            </div>
        );
    }

    render() {
        const { modal: { isOpen } } = this.props;
        if (!isOpen) return null;

        let classes = 'modal';
        if (isOpen) classes += ' modal--open';

        return (
            <div className={classes}>
                {this.renderOverlay()}
                {this.renderBox()}
            </div>
        );
    }
}
