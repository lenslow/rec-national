import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Modal from './components/modal';
import * as modalActions from './state/modal-action-creators';

function select(state) {
    return {
        modal: state.modal,
    };
}

function ModalConnector(props) {
    const { dispatch } = props;
    return (
        <Modal
            {...props}
            {...bindActionCreators(modalActions, dispatch)}
        />
    );
}

export default connect(select)(ModalConnector);
