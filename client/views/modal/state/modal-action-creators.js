import modalActionTypes from './modal-action-types';

export function modalOpened(modalData) {
    return {
        type: modalActionTypes.MODAL_OPENED,
        data: { ...modalData },
    };
}

export function modalClosed() {
    return {
        type: modalActionTypes.MODAL_CLOSED,
    };
}

export function modalLocked() {
    return {
        type: modalActionTypes.MODAL_LOCKED,
    };
}

export function modalUnlocked() {
    return {
        type: modalActionTypes.MODAL_UNLOCKED,
    };
}
