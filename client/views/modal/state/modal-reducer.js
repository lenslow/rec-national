import createReducer from '../../../util/create-reducer';
import modalActionTypes from './modal-action-types';

const initialState = {
    isOpen: false,

    content: null,
    className: '',
    locked: false,
};

export default createReducer(initialState, {
    [modalActionTypes.MODAL_OPENED](state, action) {
        const { data: { content, className, locked } } = action;

        return {
            ...state,
            isOpen: true,
            content,
            className,
            locked,
        };
    },

    [modalActionTypes.MODAL_CLOSED](state, action) {
        return {
            ...initialState,
        };
    },

    [modalActionTypes.MODAL_LOCKED](state) {
        return {
            ...state,
            locked: true,
        };
    },

    [modalActionTypes.MODAL_UNLOCKED](state) {
        return {
            ...state,
            locked: false,
        };
    },
});
