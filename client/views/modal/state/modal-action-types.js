import createActionTypes from '../../../util/create-action-types';

const modalActionTypes = createActionTypes([
    'MODAL_OPENED',
    'MODAL_CLOSED',
    'MODAL_LOCKED',
    'MODAL_UNLOCKED',
]);

export default modalActionTypes;
