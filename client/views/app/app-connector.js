import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { GlobalHeader, GlobalFooter, Modal } from '../../views';
import './app.scss';

function select(state) {
    return {

    };
}

class AppConnector extends Component {

    static propTypes = {
        children: PropTypes.node,
    };

    render() {
        const { children } = this.props;

        return (
            <div className='wrapper'>
                <GlobalHeader />
                <main className='app'>
                    {children}
                    <GlobalFooter />
                </main>

                {/* outside elements here (modal, popup boxes, etc.) */}
                <Modal />
            </div>
        );
    }
}

export default connect(select)(AppConnector);
