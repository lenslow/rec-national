import React, { Component, PropTypes } from 'react';
import './input.scss';

export default class Input extends Component {

    static propTypes = {
        id: PropTypes.string.isRequired,
        className: PropTypes.string,

        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]).isRequired,
        placeholder: PropTypes.string,
        type: PropTypes.oneOf(['text', 'email', 'number', 'phone', 'password']),
        onChange: PropTypes.func.isRequired,

        label: PropTypes.string.isRequired,
        hasLabel: PropTypes.bool,
    };

    static defaultProps = {
        type: 'text',
        hasLabel: true,
    };

    handleChange = (e) => {
        const { onChange } = this.props;
        onChange(e.target.value);
    };

    renderLabel() {
        const { id, hasLabel, label } = this.props;
        let classes = 'input__label';
        if (!hasLabel) classes += ' input__label--hidden';

        return (
            <label htmlFor={id} className={classes}>{label}</label>
        );
    }

    renderField() {
        const { value, placeholder, type, id } = this.props;

        return (
            <input
                id={id}
                className='input__field'

                value={value}
                placeholder={placeholder}
                type={type}

                onChange={this.handleChange}
            />
        );
    }

    render() {
        const { className } = this.props;
        return (
            <div className={`input ${className || ''}`}>
                {this.renderLabel()}
                {this.renderField()}
            </div>
        );
    }
}
