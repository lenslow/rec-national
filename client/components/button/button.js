import React, { Component, PropTypes } from 'react';
import './button.scss';

export default class Input extends Component {

    static propTypes = {
        className: PropTypes.string,
        text: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        children: PropTypes.any,
        type: PropTypes.string,
        hidden: PropTypes.bool,
        disabled: PropTypes.bool,
    };

    static defaultProps = {
        type: 'button',
        hidden: false,
        disabled: false,
    };

    render() {
        const { className, onClick, text, type, children, hidden, disabled } = this.props;
        const textClass = type === 'text' ? 'button--text' : '';
        const hiddenClass = hidden ? 'button--hidden' : '';

        return (
            <button
                className={`button ${textClass} ${hiddenClass} ${className}`}
                onClick={onClick}
                disabled={disabled}
            >
                {text || children}
            </button>
        );
    }
}
