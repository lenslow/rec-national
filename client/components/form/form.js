import React, { Component, PropTypes } from 'react';
import { Input } from '../';

import './form.scss';

export default class Form extends Component {

    static propTypes = {
        fields: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
            hasLabel: PropTypes.bool,
            placeholder: PropTypes.string,
            value: PropTypes.string.isRequired,
            onChange: PropTypes.func.isRequired,
            type: PropTypes.string,
        })).isRequired,
        onSubmit: PropTypes.func.isRequired,
        submitLabel: PropTypes.string,
        submitError: PropTypes.string,
        className: PropTypes.string,
    };

    render() {
        const { fields, onSubmit, submitLabel, submitError, className } = this.props;
        const formClasses = ['form'];
        const submitErrorClass = submitError === 'Forbidden' ? 'submit__error' : null;

        if (className) {
            formClasses.push(className);
        }

        return (
            <form className={formClasses.join(' ')} onSubmit={this.handleSubmit}>
                {fields.map((field, i) => (
                    <Input
                        key={i}
                        id={field.id}
                        label={field.label}
                        hasLabel={field.hasLabel}
                        placeholder={field.placeholder}
                        value={field.value}
                        onChange={field.onChange}
                        type={field.type}
                    />
                ))}

                <button
                    disabled={submitError === 'Forbidden'}
                    className={submitErrorClass}
                    onClick={onSubmit}
                >
                    {submitLabel || 'Submit'}
                </button>
            </form>
        );
    }
}
