import React, { PropTypes, Component } from 'react';
import './administration-container.scss';

export default class AdministrationContainer extends Component {

    static propTypes = {
        children: PropTypes.node.isRequired,
        title: PropTypes.string,
    };

    renderTitle() {
        const { title } = this.props;
        if (!title) return null;

        return (
            <div className='administration-container__title-wrapper'>
                <h2 className='administration-container__title'>{title}</h2>
            </div>
        );
    }

    render() {
        const { children } = this.props;

        return (
            <div className='administration-container'>
                {this.renderTitle()}
                {children}
            </div>
        );
    }
}
