import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { syncHistory, routeReducer } from 'react-router-redux';
import { browserHistory } from 'react-router';
import AppRouter from './app-router';
import * as reducers from './state/reducers';
import { loggingMiddleware } from './middleware';
import thunk from 'redux-thunk';
import configureAjax from './util/ajax';

/**
 * Global Styles
 */
import './assets/style/global.scss';
import './assets/style/no-javascript.css'; // import for webpack build

/**
 * Store Creation
 */
const combinedReducers = combineReducers(Object.assign({}, reducers, {
    routing: routeReducer,
}));

// Sync dispatched route actions to the history
const reduxRouterMiddleware = syncHistory(browserHistory);

// Create store with middleware
const store = createStore(
	combinedReducers,
	applyMiddleware(
		reduxRouterMiddleware,
		thunk,
		loggingMiddleware,
	),
);

// Required for replaying actions from devtools to work
reduxRouterMiddleware.listenForReplays(store);

/**
 * Additional Setup
 */
configureAjax();

ReactDOM.render(
    <Provider store={store}>
        <AppRouter store={store} />
    </Provider>,
    document.getElementById('app')
);
