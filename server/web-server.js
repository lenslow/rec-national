// web server

import express from 'express';
import path from 'path';
import passportSetup from './passport-setup';
import session from 'express-session';
import bodyParser from 'body-parser';

// Initialize app
const app = express();
app.use(session({ secret: 't3st1cu1ar-tort10n', resave: false, saveUninitialized: false }));

// body-parser for JSON body parsing
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());

// IMPORTANT --- THIS IS TEMPORARY, need to set actual allowed origins
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH');
    next();
});

// Initialize server
import http from 'http';
const httpServer = http.Server(app);

// Setup logging
import log from 'morgan';
app.use(log('dev'));

// Import routes
import routes from './routes/main.js';
passportSetup(app);
routes(app, express);

// Use distribution folder in production
process.env.NODE_ENV === 'production' ?
    app.use(express.static(path.join(__dirname, '../../dist'))) :
    app.use('/', express.static(path.join(__dirname, '/')));

// Start server
const port = process.env.PORT || 8080;
httpServer.listen(port);
console.log(`Web server started on ${port}`);
