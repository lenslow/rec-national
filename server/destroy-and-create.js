import knex from './db';
knex.schema.raw('DROP SCHEMA public CASCADE').then(() => {
    knex.schema.raw('CREATE SCHEMA public').then(() => {
        knex.migrate.latest().then(() => process.exit());
    });
});
