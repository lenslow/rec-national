import knex from '../db';
import { generateToken } from '../util/jwt-token-util';
import { sendResponse } from '../util/response-util';

const userInfo = ['users.email', 'users.firstName', 'users.lastName', 'users.hasLoggedIn', 'users.hasVerifiedEmail', 'users.artistId', 'users.id as userId'];
const artistInfo = ['artist.name as artistName', 'artist.email as artistEmail', 'artist.about as artistAbout'];

export function login(req, res, user) {
    // manage login flag
    const { hasLoggedIn } = user;
    if (!hasLoggedIn) {
        knex('users')
            .where({ id: user.id })
            .update({ hasLoggedIn: true })
            .then(() => sendUserWithToken(res, user)); // do not send the updated user
    } else {
        sendUserWithToken(res, user); // TODO: this user has all user information.
    }
}

export function requestUser(req, res) {
    knex('users')
        .first()
        .innerJoin('artist', 'users.artistId', 'artist.id')
        .where({ 'users.email': req.userEmail })
        .select([].concat(userInfo, artistInfo))
        .then(user => {
            if (user === null) {
                res.status(401);
                res.json({
                    message: 'User not found.',
                });
            } else {
                sendUserWithToken(res, user);
            }
        }
    );
}

/**
 * Utils
 */

function sendUserWithToken(res, user) {
    const token = generateToken({ email: user.email }, 60, 'authentication');
    sendResponse(res, 200, { user, token });
}
