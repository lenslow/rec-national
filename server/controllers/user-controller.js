import knex from '../db';
import _ from 'underscore';
import { sendResponse } from '../util/response-util';

export function updateUser(req, res) {
    const { userId } = req.params;
    if (_.isUndefined(userId)) {
        sendResponse(res, 403, { message: 'User id must be provided.' });
    }

    const {
        firstName,
        lastName,
        phoneNumber,
    } = req.body;

    const userUpdates = {
        updatedAt: knex.raw('now()'),
    };

    // TODO: validate incoming data
    if (firstName) userUpdates.firstName = firstName;
    if (lastName) userUpdates.lastName = lastName;
    if (phoneNumber) userUpdates.phoneNumber = phoneNumber;

    knex('users')
        .where({ id: userId })
        .update(userUpdates)
        .then((user) => {
            sendResponse(res, 200, { message: 'User updated.' });
        })
        .catch(error => {
            // TODO: handle user update errors
            sendResponse(res, 403, { message: 'User update error.', error });
        });
}
