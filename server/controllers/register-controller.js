import knex from '../db';
import { generateHashedPassword } from '../util/password-util';
import { sendResponse } from '../util/response-util';
import { clientUrl } from '../util/config';
import { emailTransporter, siteEmail } from '../util/email-util';
import { generateToken } from '../util/jwt-token-util';
import { EMAIL_RGX, PASSWORD_RGX } from '../../shared-utils/regex';

export function register(req, res) {
    const email = req.body.email;
    const password = req.body.password;
    const confirmPassword = req.body.confirmPassword;
    const isEmailValid = EMAIL_RGX.test(email);
    const isPasswordValid = PASSWORD_RGX.test(password);

    if (!email || !password) {
        sendResponse(res, 403, { message: 'Email and password required.' });
        return;
    }

    if (!isEmailValid) {
        sendResponse(res, 403, { message: 'Invalid Email.' });
        return;
    }

    if (!isPasswordValid) {
        sendResponse(res, 403, {
            message: 'Invalid password. Passwords must be a minimum of 8 characters.',
        });
        return;
    }

    if (password !== confirmPassword) {
        sendResponse(res, 403, { message: 'Passwords do not match.' });
        return;
    }

    knex('artist')
        .insert({})
        .returning('id')
        .then(([artistId]) => {
            if (!artistId) sendResponse(res, 403, { message: 'Artist creation failed.' });

            const hashedPasswordInfo = generateHashedPassword(password);
            const newUser = {
                email,
                salt: hashedPasswordInfo.salt,
                password: hashedPasswordInfo.password,
                artistId, // set artist foreign key
            };

            knex('users')
                .insert(newUser)
                .then(() => {
                    sendVerificationEmail(email, res);
                })
                .catch(error => {
                    // TODO: If user creation fails, need to delete artist entry
                    sendResponse(res, 403, { message: 'Registration error.', error });
                });
        });
}

function sendVerificationEmail(email, res) {
    const resetToken = generateToken({ email }, 15, 'emailVerification');
    const url = `${clientUrl}/email-verification?t=${encodeURIComponent(resetToken)}`;
    const html = `<a target="_blank" href="${url}">Click here to verify your email address.</a>`;

    const options = {
        from: siteEmail,
        to: email,
        subject: 'Raspy Dev Email Verification',
        html,
    };

    emailTransporter.sendMail(options, (error, info) => {
        if (error) {
            res.status(404); // TODO
            res.json({
                error,
            });
        } else {
            sendResponse(res, 200, { message: 'User created and email verification sent.' });
        }
    });
}
