import _ from 'underscore';
import http from 'http';
import https from 'https';
import { sendResponse } from '../util/response-util';
const googleApiKey = 'AIzaSyDprtPtgLQsvhooBATCWT6AgdXGqaXNbiY';

export function getLocation(req, res) {
    const { params: { location } } = req;
    if (_.isUndefined(location)) {
        sendResponse(res, 403, { message: 'Location string must be provided.' });
    }

    makeGoogleGeocoderRequest(encodeURI(location))
        .then(geocodeResponse => {
            const { status, results } = geocodeResponse;
            if (status === 'OK' && Array.isArray(results)) {
                sendResponse(res, 200, results);
            } else {
                sendResponse(res, 404, { error: 'TODO: // give better error' });
            }
        }).catch(error => {
            // TODO: handle promise error
            sendResponse(res, 404, { error });
        });
}

// TODO: figure out http vs. https stuff

function makeGoogleGeocoderRequest(location) {
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${location}&key=${googleApiKey}`;

    return new Promise((resolve, reject) => {
        https.get(url, response => {
            let data = '';
            response.on('data', (d) => data += d);
            response.on('end', () => resolve(JSON.parse(data)));
        })
        .on('error', error => reject(error));
    });
}
