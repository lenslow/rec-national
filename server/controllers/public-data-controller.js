import knex from '../db';
import _ from 'underscore';
import { sendResponse } from '../util/response-util';

const publicProfileInformation = [
    'users.firstName',
    'users.lastName',
    'users.phoneNumber',
    'artist.name',
    'artist.email',
    'artist.about',
];

export function getUserForPublicProfile(req, res) {
    const { userId } = req.params;
    if (_.isUndefined(userId)) {
        sendResponse(res, 403, { message: 'User id must be provided.' });
    }

    knex('users')
        .first()
        .innerJoin('artist', 'users.artistId', 'artist.id')
        .where({ 'users.id': userId })
        .select(publicProfileInformation)
        .then(user => {
            if (user === null) {
                sendResponse(res, 403, { message: 'User not found.' });
            }

            sendResponse(res, 200, { ...user });
        });
}
