import knex from '../db';
import { validateToken } from '../util/jwt-token-util';
import { sendResponse } from '../util/response-util';

export function verifyEmail(req, res) {
    const { body: { email, emailVerificationToken } } = req;
    // TODO: validate information

    const isTokenValidInfo = validateToken(emailVerificationToken, 'emailVerification', email, res);
    if (!isTokenValidInfo.isValid) return; // TODO: this doesn't make sense

    const updatedUser = {
        hasVerifiedEmail: true,
    };

    // TODO: handle errors
    knex('users')
        .first()
        .where({ email })
        .update(updatedUser)
        .then(() => sendResponse(res, 200, { message: 'Email verified.' }));
}
