import knex from '../db';

export function getArtistCategories(req, res) {
    knex.select().from('artistCategories')
        .then(categories => {
            if (categories === null) {
                res.status(404);
                res.json({
                    message: 'Categories not found.',
                });
            } else {
                res.status(200);
                res.json(categories);
            }
        });
}
