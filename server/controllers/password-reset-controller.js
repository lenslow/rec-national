import knex from '../db';
import { clientUrl } from '../util/config';
import { generateHashedPassword } from '../util/password-util';
import { emailTransporter, siteEmail } from '../util/email-util';
import { generateToken, validateToken } from '../util/jwt-token-util';

export function requestPasswordReset(req, res) {
    const email = req.body.email;

    // check for that email in the system
    // - if not found, 401
    knex('users').first().where({ email })
        .then(user => {
            if (!user) {
                res.status(401);
                res.json({
                    message: 'User not found.',
                });
            } else {
                sendResetTokenEmail(email, res);
            }
        }
    );
}

export function resetPassword(req, res) {
    const newPassword = req.body.newPassword;
    const email = req.body.email;
    const resetToken = req.body.resetToken;
    const isTokenValidInfo = validateToken(resetToken, 'reset', email, res);
    if (!isTokenValidInfo.isValid) return; // TODO: this doesn't make sense

    // hash password and update the user password and salt
    const hashedPasswordInfo = generateHashedPassword(newPassword);
    const updatedUser = {
        salt: hashedPasswordInfo.salt,
        password: hashedPasswordInfo.password,
    };

    // TODO: handle errors
    knex('users').first().where({ email }).update(updatedUser)
        .then(() => {
            res.status(200);
            res.json({
                message: 'Password reset succeeded.',
            });
        }
    );
}


/**
 * Utilities
 */

function sendResetTokenEmail(email, res) {
    const resetToken = generateToken({ email }, 15, 'reset');
    const url = `${clientUrl}/password-reset?email=${encodeURIComponent(email)}&resetToken=${encodeURIComponent(resetToken)}`;
    const html = `<a target="_blank" href="${url}">Click here to reset your password.</a>`;

    const options = {
        from: siteEmail,
        to: email,
        subject: 'Raspy Dev Password Reset',
        html,
    };

    emailTransporter.sendMail(options, (error, info) => {
        if (error) {
            res.status(404); // ??
            res.json({
                error,
            });
        } else {
            res.status(200);
            res.json({
                message: 'Email sent',
            });
        }
    });
}
