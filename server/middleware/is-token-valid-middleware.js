import { validateToken } from '../util/jwt-token-util';

export default function isTokenValidMiddleware(req, res, next) {
    const { headers: { authorization: token } } = req;
    const isTokenValidInfo = validateToken(token, 'authentication', undefined, res);

    req.userEmail = isTokenValidInfo.tokenEmail;
    next();
}
