import bcrypt from 'bcryptjs';

export function generateHashedPassword(stringPassword) {
    const salt = bcrypt.genSaltSync(10);
    const password = bcrypt.hashSync(stringPassword, salt);
    return { salt, password };
}
