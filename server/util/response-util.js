
export function sendResponse(responseObject, status = 200, data = {}) {
    responseObject.status(status).send(data);
}
