import jwt from 'jwt-simple';
import * as secrets from './secrets-util';

export function generateToken(data, expirationInMinutes, type) {
    return jwt.encode({
        ...data,
        expires: getDatePlusMinutes(expirationInMinutes),
    }, getSecret(type));
}

export function validateToken(token, type, emailToVerify, response) {
    // check for token existence
    if (!token) {
        sendUnauthorized(response, 'No token');
        return false;
    }

    // check valid token (will have 3 segments separated by a period)
    const segments = token.split('.');
    if (segments.length !== 3) {
        sendUnauthorized(response, 'Invalid reset token.');
        return false;
    }

    // decode and check that token is not expired
    const decoded = jwt.decode(token, getSecret(type));
    if (decoded.expires <= Date.now()) {
        sendUnauthorized(response, 'Token is expired.');
        return false;
    }

    if (emailToVerify) {
        // Check that recieved email (should be logged in user email) and
        // email stored in token are the same.
        if (emailToVerify !== decoded.email) {
            sendUnauthorized(response, 'Unauthorized');
            return false;
        }
    }

    return {
        isValid: true,
        tokenEmail: decoded.email,
    };
}

function getSecret(type) {
    switch (type) {
        case 'authentication':
            return secrets.authentication;
        case 'reset':
            return secrets.reset;
        case 'emailVerification':
            return secrets.emailVerification;
        default:
            return null;
    }
}

function getDatePlusMinutes(minutes) {
    const date = new Date();
    return new Date(date.getTime() + (minutes * 60000));
}

function sendUnauthorized(res, message = 'Unauthorized') {
    res.status(401);
    res.json({
        message,
    });
}
