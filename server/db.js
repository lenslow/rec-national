import config from '../knexfile.js';
import knex from 'knex';

const env = 'development';

export default knex(config[env]);

//knex(config[env]).migrate.latest([config]);
