import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import bcrypt from 'bcryptjs';
import knex from './db';

export default function (app) {
    app.use(passport.initialize());
    app.use(passport.session());

    passport.use(new LocalStrategy(

        {
            usernameField: 'email',
            passwordField: 'password',
        },

        (email, password, done) => {
            knex('users').first().where({ email })
                .then(user => {

                    if (!user || user === null) {
                        return done(null, false, { message: 'Incorrect email.' });
                    }

                    const hashedPassword = bcrypt.hashSync(password, user.salt);

                    // compare passwords
                    if (user.password === hashedPassword) {
                        return done(null, user);
                    }

                    return done(null, false, { message: 'Incorrect password.' });
                });
        }
    ));

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        knex('users').where({ id })
            .then(user => {
                if (user === null) {
                    done(new Error('Incorrect user id.'));
                }

                done(null, user);
            });
    });
}
