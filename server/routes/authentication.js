import passport from 'passport';
import { register } from '../controllers/register-controller';
import isTokenValidMiddleware from '../middleware/is-token-valid-middleware';
import { login, requestUser } from '../controllers/authentication-controller';
import { requestPasswordReset, resetPassword } from '../controllers/password-reset-controller';
import { verifyEmail } from '../controllers/email-verification-controller';
import { sendResponse } from '../util/response-util';

export default function (app, express) {

    const authenticationRouter = express.Router();

    /**
     * Open Routes
     */

    authenticationRouter.post('/register', register);
    authenticationRouter.post('/login', (req, res, next) => {
        passport.authenticate('local', (error, user, info) => {
            if (error) {
                next(error);
                return;
            }

            if (!user) {
                sendResponse(res, 401, { message: info.message });
                return;
            }

            if (user) {
                login(req, res, user);
                return;
            }
        })(req, res, next);
    });
    authenticationRouter.get('/logout', (req, res) => req.logout);
    authenticationRouter.post('/request-password-reset', requestPasswordReset);
    authenticationRouter.post('/password-reset', resetPassword);

    /**
     * Secured Routes
     */

    authenticationRouter.get('/user', isTokenValidMiddleware, requestUser);
    authenticationRouter.post('/verify-email', isTokenValidMiddleware, verifyEmail);

    // route family path
    app.use('/api/authentication/', authenticationRouter);
}
