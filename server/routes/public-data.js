import isTokenValidMiddleware from '../middleware/is-token-valid-middleware';
import { getUserForPublicProfile } from '../controllers/public-data-controller';

export default function (app, express) {

	const publicDataRouter = express.Router();

	/**
	 * Secured Routes
	 */

	publicDataRouter.get('/:userId', isTokenValidMiddleware, getUserForPublicProfile);

	// route family path
	app.use('/api/public-data/', publicDataRouter);
}
