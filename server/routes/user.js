import isTokenValidMiddleware from '../middleware/is-token-valid-middleware';
import { updateUser } from '../controllers/user-controller';

export default function (app, express) {

    const userRouter = express.Router();

    /**
     * Secured Routes
     */

    userRouter.patch('/:userId', isTokenValidMiddleware, updateUser);

    // route family path
    app.use('/api/user/', userRouter);
}
