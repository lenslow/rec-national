import isTokenValidMiddleware from '../middleware/is-token-valid-middleware';
import { getLocation } from '../controllers/location-controller';

export default function (app, express) {

    const locationRouter = express.Router();

    /**
     * Secured Routes
     */

    // TODO: do we need to have this secured?
    locationRouter.get('/:location', isTokenValidMiddleware, getLocation);

    // route family path
    app.use('/api/location/', locationRouter);
}
