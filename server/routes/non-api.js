import path from 'path';

export default function (app, express) {

    const nonAPIRouter = express.Router();

    // catch all to send index to client (production only)
    nonAPIRouter.get('*', (req, res) => {
        if (process.env.NODE_ENV === 'production') {
            res.sendFile('index.html', {
                root: path.join(__dirname, '../../dist/'),
            });
        }
    });

    app.use('/', nonAPIRouter);
}
