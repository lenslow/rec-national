// import and initialize all routers
import authentication from './authentication';
import user from './user';
import publicData from './public-data';
import location from './location';
import nonapi from './non-api';

export default function (app, express) {
    authentication(app, express);
    user(app, express);
    publicData(app, express);
    location(app, express);
    nonapi(app, express);
}
